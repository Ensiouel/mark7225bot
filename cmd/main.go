package main

import (
	"Mark7225Bot/internal/message"
	"Mark7225Bot/internal/repository"
	"context"
	"log/slog"
	"os"
	"os/signal"

	"github.com/and3rson/telemux/v2"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/jackc/pgx/v5/pgxpool"
)

var (
	TelegramBotToken = os.Getenv("TELEGRAM_BOT_TOKEN")
	PostgresURL      = os.Getenv("POSTGRES_URL")
)

func main() {
	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancel()

	slog.Info("init db", slog.String("url", PostgresURL))
	db, err := initDB(ctx)
	if err != nil {
		slog.Error("failed to init db")
		return
	}

	slog.Info("init telegram api")
	bot, err := tgbotapi.NewBotAPI(TelegramBotToken)
	if err != nil {
		slog.Error("failed to create bot", slog.Any("error", err))
		return
	}

	messagesRepository := repository.NewMessagesRepository(db)
	builder := message.NewBuilder(messagesRepository)

	mux := telemux.NewMux().
		AddHandler(
			telemux.NewMessageHandler(telemux.And(telemux.Or(telemux.HasText(), telemux.HasSticker()), telemux.Not(telemux.IsPrivate())), func(update *telemux.Update) {
				effectiveMessage := update.EffectiveMessage()

				if effectiveMessage.From.IsBot {
					return
				}

				err := messagesRepository.Create(ctx, repository.Message{
					MessageID: effectiveMessage.MessageID,
					UserID:    effectiveMessage.From.ID,
					UserName:  effectiveMessage.From.String(),
					ChatID:    effectiveMessage.Chat.ID,
					Text:      effectiveMessage.Text,
					IsSticker: effectiveMessage.Sticker != nil,
					Date:      effectiveMessage.Time(),
				})
				if err != nil {
					slog.Warn("failed to create message", slog.Any("error", err))
					return
				}
			}),
			telemux.NewCommandHandler("total_messages", telemux.IsPrivate(), func(update *telemux.Update) {
				chattable, err := builder.BuildTotalByMessages(ctx, update)
				if err != nil {
					slog.Warn("failed to build message", slog.String("command", "total_message"), slog.Any("error", err))
					return
				}

				_, err = bot.Send(chattable)
				if err != nil {
					slog.Warn("failed to send message", slog.String("command", "total_message"), slog.Any("error", err))
					return
				}
			}),
			telemux.NewCommandHandler("total_words", telemux.IsPrivate(), func(update *telemux.Update) {
				chattable, err := builder.BuildTotalByWords(ctx, update)
				if err != nil {
					slog.Warn("failed to build message", slog.String("command", "total_word"), slog.Any("error", err))
					return
				}

				_, err = bot.Send(chattable)
				if err != nil {
					slog.Warn("failed to send message", slog.String("command", "total_word"), slog.Any("error", err))
					return
				}
			}),
			telemux.NewCommandHandler("total_stickers", telemux.IsPrivate(), func(update *telemux.Update) {
				chattable, err := builder.BuildTotalByStickers(ctx, update)
				if err != nil {
					slog.Warn("failed to build message", slog.String("command", "total_stickers"), slog.Any("error", err))
					return
				}

				_, err = bot.Send(chattable)
				if err != nil {
					slog.Warn("failed to send message", slog.String("command", "total_stickers"), slog.Any("error", err))
					return
				}
			}),
			telemux.NewCommandHandler("total_word_frequency", telemux.IsPrivate(), func(update *telemux.Update) {
				chattable, err := builder.BuildTotalWordFrequency(ctx, update)
				if err != nil {
					slog.Warn("failed to build message", slog.String("command", "total_stickers"), slog.Any("error", err))
					return
				}

				_, err = bot.Send(chattable)
				if err != nil {
					slog.Warn("failed to send message", slog.String("command", "total_stickers"), slog.Any("error", err))
					return
				}
			}),
			telemux.NewCallbackQueryHandler(message.QueryRefreshTotalByMessages, telemux.Any(), func(update *telemux.Update) {
				_, _ = bot.Send(tgbotapi.NewCallback(update.CallbackQuery.ID, ""))

				chattable, err := builder.BuildTotalByMessages(ctx, update, message.WithEdit())
				if err != nil {
					slog.Warn("failed to build message", slog.String("query", message.QueryRefreshTotalByMessages), slog.Any("error", err))
					return
				}

				_, err = bot.Send(chattable)
				if err != nil {
					slog.Warn("failed to send message", slog.String("query", message.QueryRefreshTotalByMessages), slog.Any("error", err))
					return
				}
			}),
			telemux.NewCallbackQueryHandler(message.QueryRefreshTotalByWords, telemux.Any(), func(update *telemux.Update) {
				_, _ = bot.Send(tgbotapi.NewCallback(update.CallbackQuery.ID, ""))

				chattable, err := builder.BuildTotalByWords(ctx, update, message.WithEdit())
				if err != nil {
					slog.Warn("failed to build message", slog.String("query", message.QueryRefreshTotalByWords), slog.Any("error", err))
					return
				}

				_, err = bot.Send(chattable)
				if err != nil {
					slog.Warn("failed to send message", slog.String("query", message.QueryRefreshTotalByWords), slog.Any("error", err))
					return
				}
			}),
			telemux.NewCallbackQueryHandler(message.QueryRefreshTotalByStickers, telemux.Any(), func(update *telemux.Update) {
				_, _ = bot.Send(tgbotapi.NewCallback(update.CallbackQuery.ID, ""))

				chattable, err := builder.BuildTotalByStickers(ctx, update, message.WithEdit())
				if err != nil {
					slog.Warn("failed to build message", slog.String("query", message.QueryRefreshTotalByStickers), slog.Any("error", err))
					return
				}

				_, err = bot.Send(chattable)
				if err != nil {
					slog.Warn("failed to send message", slog.String("query", message.QueryRefreshTotalByStickers), slog.Any("error", err))
					return
				}
			}),
			telemux.NewCallbackQueryHandler(message.QueryRefreshTotalWordFrequency, telemux.Any(), func(update *telemux.Update) {
				_, _ = bot.Send(tgbotapi.NewCallback(update.CallbackQuery.ID, ""))

				chattable, err := builder.BuildTotalWordFrequency(ctx, update, message.WithEdit())
				if err != nil {
					slog.Warn("failed to build message", slog.String("query", message.QueryRefreshTotalWordFrequency), slog.Any("error", err))
					return
				}

				_, err = bot.Send(chattable)
				if err != nil {
					slog.Warn("failed to send message", slog.String("query", message.QueryRefreshTotalWordFrequency), slog.Any("error", err))
					return
				}
			}),
		)

	slog.Info("bot started")

	updateConfig := tgbotapi.NewUpdate(0)
	updates := bot.GetUpdatesChan(updateConfig)

	for update := range updates {
		mux.Dispatch(bot, update)
	}
}

func initDB(ctx context.Context) (*pgxpool.Pool, error) {
	db, err := pgxpool.New(ctx, PostgresURL)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func init() {
	slog.SetDefault(slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug})))
}
