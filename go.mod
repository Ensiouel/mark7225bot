module Mark7225Bot

go 1.21.4

toolchain go1.21.6

require (
	github.com/and3rson/telemux/v2 v2.0.2
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
	github.com/jackc/pgx/v5 v5.5.2
)

require (
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/puddle/v2 v2.2.1 // indirect
	github.com/stretchr/testify v1.8.4 // indirect
	golang.org/x/crypto v0.17.0 // indirect
	golang.org/x/sync v0.6.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
