-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS message
(
    message_id BIGINT PRIMARY KEY,
    user_id    BIGINT      NOT NULL,
    chat_id    BIGINT      NOT NULL,
    text       TEXT        NOT NULL,
    date       TIMESTAMPTZ NOT NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS message;
-- +goose StatementEnd
