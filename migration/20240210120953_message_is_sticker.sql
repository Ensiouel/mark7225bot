-- +goose Up
-- +goose StatementBegin
ALTER TABLE message
    ADD COLUMN IF NOT EXISTS is_sticker BOOLEAN;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
SELECT 'down SQL query';
-- +goose StatementEnd
