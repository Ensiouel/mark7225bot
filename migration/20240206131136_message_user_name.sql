-- +goose Up
-- +goose StatementBegin
ALTER TABLE message
    ADD COLUMN IF NOT EXISTS user_name TEXT;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
SELECT 'down SQL query';
-- +goose StatementEnd
