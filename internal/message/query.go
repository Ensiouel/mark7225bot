package message

const (
	QueryRefreshTotalByMessages    = "refresh_total_by_messages"
	QueryRefreshTotalByWords       = "refresh_total_by_words"
	QueryRefreshTotalByStickers    = "refresh_total_by_stickers"
	QueryRefreshTotalWordFrequency = "refresh_total_word_frequency"
)
