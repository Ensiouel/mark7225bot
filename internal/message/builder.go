package message

import (
	"Mark7225Bot/internal/repository"
	"context"
	"fmt"

	"github.com/and3rson/telemux/v2"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type Builder struct {
	messages *repository.MessagesRepository
}

func NewBuilder(messages *repository.MessagesRepository) *Builder {
	return &Builder{messages: messages}
}

func (b *Builder) BuildTotalByMessages(ctx context.Context, update *telemux.Update, options ...Option) (tgbotapi.Chattable, error) {
	cfg := newConfig(options...)

	totalMessages, err := b.messages.SelectTotalByMessages(ctx)
	if err != nil {
		return nil, err
	}

	text := "Топ участников по количеству отправленных сообщений\n"
	for i, totalMessage := range totalMessages {
		text += fmt.Sprintf("%d. %s: %d\n", i+1, totalMessage.Field, totalMessage.Count)
	}
	keyboard := tgbotapi.NewInlineKeyboardMarkup(tgbotapi.NewInlineKeyboardRow(tgbotapi.NewInlineKeyboardButtonData("🔄", QueryRefreshTotalByMessages)))

	var (
		chattable tgbotapi.Chattable
		message   = update.EffectiveMessage()
		chat      = message.Chat
	)

	if cfg.edit {
		editMessageTextConfig := tgbotapi.NewEditMessageTextAndMarkup(
			chat.ID,
			message.MessageID,
			text,
			keyboard,
		)
		chattable = editMessageTextConfig
	} else {
		messageConfig := tgbotapi.NewMessage(message.Chat.ID, text)
		messageConfig.ReplyMarkup = keyboard
		chattable = messageConfig
	}

	return chattable, nil
}

func (b *Builder) BuildTotalByWords(ctx context.Context, update *telemux.Update, options ...Option) (tgbotapi.Chattable, error) {
	cfg := newConfig(options...)

	totalMessages, err := b.messages.SelectTotalByWords(ctx)
	if err != nil {
		return nil, err
	}

	text := "Топ участников по количеству отправленных слов\n"
	for i, totalMessage := range totalMessages {
		text += fmt.Sprintf("%d. %s: %d\n", i+1, totalMessage.Field, totalMessage.Count)
	}
	keyboard := tgbotapi.NewInlineKeyboardMarkup(tgbotapi.NewInlineKeyboardRow(tgbotapi.NewInlineKeyboardButtonData("🔄", QueryRefreshTotalByWords)))

	var (
		chattable tgbotapi.Chattable
		message   = update.EffectiveMessage()
		chat      = message.Chat
	)

	if cfg.edit {
		editMessageTextConfig := tgbotapi.NewEditMessageTextAndMarkup(
			chat.ID,
			message.MessageID,
			text,
			keyboard,
		)
		chattable = editMessageTextConfig
	} else {
		messageConfig := tgbotapi.NewMessage(message.Chat.ID, text)
		messageConfig.ReplyMarkup = keyboard
		chattable = messageConfig
	}

	return chattable, nil
}

func (b *Builder) BuildTotalByStickers(ctx context.Context, update *telemux.Update, options ...Option) (tgbotapi.Chattable, error) {
	cfg := newConfig(options...)

	totalMessages, err := b.messages.SelectTotalByStickers(ctx)
	if err != nil {
		return nil, err
	}

	text := "Топ участников по количеству отправленных стикеров\n"
	for i, totalMessage := range totalMessages {
		text += fmt.Sprintf("%d. %s: %d\n", i+1, totalMessage.Field, totalMessage.Count)
	}
	keyboard := tgbotapi.NewInlineKeyboardMarkup(tgbotapi.NewInlineKeyboardRow(tgbotapi.NewInlineKeyboardButtonData("🔄", QueryRefreshTotalByStickers)))

	var (
		chattable tgbotapi.Chattable
		message   = update.EffectiveMessage()
		chat      = message.Chat
	)

	if cfg.edit {
		editMessageTextConfig := tgbotapi.NewEditMessageTextAndMarkup(
			chat.ID,
			message.MessageID,
			text,
			keyboard,
		)
		chattable = editMessageTextConfig
	} else {
		messageConfig := tgbotapi.NewMessage(message.Chat.ID, text)
		messageConfig.ReplyMarkup = keyboard
		chattable = messageConfig
	}

	return chattable, nil
}

func (b *Builder) BuildTotalWordFrequency(ctx context.Context, update *telemux.Update, options ...Option) (tgbotapi.Chattable, error) {
	cfg := newConfig(options...)

	totalWordFrequency, err := b.messages.SelectTotalWordFrequency(ctx)
	if err != nil {
		return nil, err
	}

	text := "Топ самых популярных слов\n"
	for i, total := range totalWordFrequency {
		text += fmt.Sprintf("%d. %s: %d\n", i+1, total.Field, total.Count)
	}
	keyboard := tgbotapi.NewInlineKeyboardMarkup(tgbotapi.NewInlineKeyboardRow(tgbotapi.NewInlineKeyboardButtonData("🔄", QueryRefreshTotalWordFrequency)))

	var (
		chattable tgbotapi.Chattable
		message   = update.EffectiveMessage()
		chat      = message.Chat
	)

	if cfg.edit {
		editMessageTextConfig := tgbotapi.NewEditMessageTextAndMarkup(
			chat.ID,
			message.MessageID,
			text,
			keyboard,
		)
		chattable = editMessageTextConfig
	} else {
		messageConfig := tgbotapi.NewMessage(message.Chat.ID, text)
		messageConfig.ReplyMarkup = keyboard
		chattable = messageConfig
	}

	return chattable, nil
}
