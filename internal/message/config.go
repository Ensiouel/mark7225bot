package message

type config struct {
	edit bool
}

type Option func(*config)

func WithEdit() Option {
	return func(config *config) {
		config.edit = true
	}
}

func newConfig(options ...Option) *config {
	cfg := &config{}
	for _, option := range options {
		option(cfg)
	}
	return cfg
}
